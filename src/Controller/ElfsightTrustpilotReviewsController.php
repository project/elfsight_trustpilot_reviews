<?php

namespace Drupal\elfsight_trustpilot_reviews\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * {@inheritdoc}
 */
class ElfsightTrustpilotReviewsController extends ControllerBase {

  /**
   * {@inheritdoc}
   */
  public function content() {
    $url = 'https://apps.elfsight.com/embed/trustpilot-reviews/?utm_source=portals&utm_medium=drupal&utm_campaign=trustpilot-reviews&utm_content=sign-up';

    require_once __DIR__ . '/embed.php';

    return [
      'response' => 1,
    ];
  }

}
